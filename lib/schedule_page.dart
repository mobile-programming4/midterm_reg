import 'package:column_widget_example/grade_page.dart';
import 'package:column_widget_example/login_page.dart';
import 'package:column_widget_example/pay_page.dart';
import 'package:column_widget_example/profile_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Schedule extends StatelessWidget {
  const Schedule({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final max_PorWidth = MediaQuery.of(context).size.width - 10;
    double max_LanWidth = MediaQuery.of(context).size.width - 10;
    return Scaffold(
        appBar: AppBar(
            title:
                Text("Reg BUU", style: TextStyle(fontWeight: FontWeight.bold)),
            backgroundColor: Colors.amber,
            actions: [
              IconButton(
                icon: Icon(Icons.logout_outlined),
                color: Colors.white,
                onPressed: () {
                  Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (context) {
                    return LoginPage();
                  }));
                },
              )
            ],
            toolbarHeight: 60,
            bottom: Tab(
                height: 45,
                child: Container(
                  width: double.infinity,
                  color: Colors.amberAccent,
                  child: ListView(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          IconButton(
                            onPressed: () {
                              Navigator.pushReplacement(context,
                                  MaterialPageRoute(builder: (context) {
                                return Profile();
                              }));
                            },
                            icon: Icon(Icons.people, color: Colors.black38),
                          ),
                          IconButton(
                            onPressed: () {
                              Navigator.pushReplacement(context,
                                  MaterialPageRoute(builder: (context) {
                                return Schedule();
                              }));
                            },
                            icon: Icon(Icons.schedule, color: Colors.black),
                          ),
                          IconButton(
                            onPressed: () {
                              Navigator.pushReplacement(context,
                                  MaterialPageRoute(builder: (context) {
                                return Grade();
                              }));
                            },
                            icon: Icon(Icons.grade, color: Colors.black38),
                          ),
                          IconButton(
                            onPressed: () {
                              Navigator.pushReplacement(context,
                                  MaterialPageRoute(builder: (context) {
                                return Pay();
                              }));
                            },
                            icon: Icon(Icons.qr_code_scanner,
                                color: Colors.black38),
                          ),
                        ],
                      )
                    ],
                  ),
                ))),
        body: Container(
            margin: EdgeInsets.all(5),
            constraints: BoxConstraints(
                maxWidth:
                    MediaQuery.of(context).orientation == Orientation.portrait
                        ? max_PorWidth
                        : max_LanWidth),
            // color: Colors.lightBlueAccent,
            child: ListView(children: [
              Row(
                children: [
                  Container(
                    margin: EdgeInsets.only(bottom: 2),
                    padding: EdgeInsets.only(left: 3, top: 5, bottom: 5),
                    color: Colors.teal[200],
                    alignment: Alignment.center,
                    width: MediaQuery.of(context).orientation ==
                            Orientation.portrait
                        ? max_PorWidth
                        : max_LanWidth,
                    // width: 380,
                    child: Text("Year 2022/2",
                        style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 16,
                            color: Colors.white)),
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5
                            : max_LanWidth / 11.5,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.black38,
                        child: Text("Day/Time",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5
                            : max_LanWidth / 11.5,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.black38,
                        child: Text("9-10",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5
                            : max_LanWidth / 11.5,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.black38,
                        child: Text("10-11",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5
                            : max_LanWidth / 11.5,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.black38,
                        child: Text("11-12",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5
                            : max_LanWidth / 11.5,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.black38,
                        child: Text("12-13",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5
                            : max_LanWidth / 11.5,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.black38,
                        child: Text("13-14",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5
                            : max_LanWidth / 11.5,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.black38,
                        child: Text("14-15",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5
                            : max_LanWidth / 11.5,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.black38,
                        child: Text("15-16",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5
                            : max_LanWidth / 11.5,
                        color: Colors.black38,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        child: Text("16-17",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5
                            : max_LanWidth / 11.5,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.black38,
                        child: Text("17-18",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5
                            : max_LanWidth / 11.5,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.black38,
                        child: Text("18-19",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5
                            : max_LanWidth / 11.5,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.black38,
                        child: Text("Mon",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5
                            : max_LanWidth / 11.5,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.black12,
                        child: Text("",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5 + max_PorWidth / 11.5 + 1
                            : max_LanWidth / 11.5 + max_LanWidth / 11.5 + 1,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.redAccent[100],
                        child: Text("88624559-59\n(IF-4M210)",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5
                            : max_LanWidth / 11.5,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.black12,
                        child: Text("",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5 + max_PorWidth / 11.5 + 1
                            : max_LanWidth / 11.5 + max_LanWidth / 11.5 + 1,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.redAccent[100],
                        child: Text("88624459-59\n(IF-3M210)",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5
                            : max_LanWidth / 11.5,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.black12,
                        child: Text("",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5
                            : max_LanWidth / 11.5,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.black12,
                        child: Text("",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5 + max_PorWidth / 11.5 + 1
                            : max_LanWidth / 11.5 + max_LanWidth / 11.5 + 1,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.redAccent[100],
                        child: Text("88624359-59\n(IF-3M210)",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5
                            : max_LanWidth / 11.5,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.black38,
                        child: Text("Tue",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5
                            : max_LanWidth / 11.5,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.black12,
                        child: Text("",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5 + max_PorWidth / 11.5 + 1
                            : max_LanWidth / 11.5 + max_LanWidth / 11.5 + 1,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.redAccent[100],
                        child: Text("88634259-59\n(IF-4C02)",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5
                            : max_LanWidth / 11.5,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.black12,
                        child: Text("",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5 + max_PorWidth / 11.5 + 1
                            : max_LanWidth / 11.5 + max_LanWidth / 11.5 + 1,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.redAccent[100],
                        child: Text("88624559-59\n(IF-3C03)",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5
                            : max_LanWidth / 11.5,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.black12,
                        child: Text("",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5
                            : max_LanWidth / 11.5,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.black12,
                        child: Text("",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5 + max_PorWidth / 11.5 + 1
                            : max_LanWidth / 11.5 + max_LanWidth / 11.5 + 1,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.redAccent[100],
                        child: Text("88624459-59\n(IF-4C01)",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5
                            : max_LanWidth / 11.5,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.black38,
                        child: Text("Wed",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5
                            : max_LanWidth / 11.5,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.black12,
                        child: Text("",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5 + max_PorWidth / 11.5 + 1
                            : max_LanWidth / 11.5 + max_LanWidth / 11.5 + 1,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.redAccent[100],
                        child: Text("88634459-59\n(IF-4C02)",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5
                            : max_LanWidth / 11.5,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.black12,
                        child: Text("",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5 + max_PorWidth / 11.5 + 1
                            : max_LanWidth / 11.5 + max_LanWidth / 11.5 + 1,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.redAccent[100],
                        child: Text("88634259-59\n(IF-4C01)",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5 + max_PorWidth / 11.5 + 1
                            : max_LanWidth / 11.5 + max_LanWidth / 11.5 + 1,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.redAccent[100],
                        child: Text("88624359-59\n(IF-3C01)",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5
                            : max_LanWidth / 11.5,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.black12,
                        child: Text("",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5
                            : max_LanWidth / 11.5,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.black12,
                        child: Text("",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5
                            : max_LanWidth / 11.5,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.black38,
                        child: Text("Thu",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5
                            : max_LanWidth / 11.5,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.black12,
                        child: Text("",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5
                            : max_LanWidth / 11.5,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.black12,
                        child: Text("",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5
                            : max_LanWidth / 11.5,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.black12,
                        child: Text("",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5
                            : max_LanWidth / 11.5,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.black12,
                        child: Text("",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5
                            : max_LanWidth / 11.5,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.black12,
                        child: Text("",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5
                            : max_LanWidth / 11.5,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.black12,
                        child: Text("",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5
                            : max_LanWidth / 11.5,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.black12,
                        child: Text("",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5
                            : max_LanWidth / 11.5,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.black12,
                        child: Text("",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5
                            : max_LanWidth / 11.5,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.black12,
                        child: Text("",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5
                            : max_LanWidth / 11.5,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.black12,
                        child: Text("",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5
                            : max_LanWidth / 11.5,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.black38,
                        child: Text("Fri",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5 +
                                max_PorWidth / 11.5 +
                                max_PorWidth / 11.5 +
                                2
                            : max_LanWidth / 11.5 +
                                max_LanWidth / 11.5 +
                                max_LanWidth / 11.5 +
                                2,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.redAccent[100],
                        child: Text("88646259-59\n(IF-5T05)",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5
                            : max_LanWidth / 11.5,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.black12,
                        child: Text("",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5 + max_PorWidth / 11.5 + 1
                            : max_LanWidth / 11.5 + max_LanWidth / 11.5 + 1,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.redAccent[100],
                        child: Text("88634459-59\n(IF-4C02)",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5
                            : max_LanWidth / 11.5,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.black12,
                        child: Text("",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5
                            : max_LanWidth / 11.5,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.black12,
                        child: Text("",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5
                            : max_LanWidth / 11.5,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.black12,
                        child: Text("",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 1, bottom: 1),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 11.5
                            : max_LanWidth / 11.5,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? max_PorWidth / 15
                            : max_LanWidth / 15,
                        color: Colors.black12,
                        child: Text("",
                            style: TextStyle(
                                fontSize: 8, fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                ],
              ),
              Container(
                  margin: EdgeInsets.only(top: 20),
                  padding: EdgeInsets.only(top: 3, bottom: 3),
                  color: Colors.red,
                  alignment: Alignment.center,
                  width:
                      MediaQuery.of(context).orientation == Orientation.portrait
                          ? max_PorWidth
                          : max_LanWidth,
                  // width: 380,
                  child: Container(
                    padding:
                        EdgeInsets.only(left: 3, top: 5, bottom: 5, right: 3),
                    color: Colors.white,
                    width: MediaQuery.of(context).orientation ==
                            Orientation.portrait
                        ? max_PorWidth
                        : max_LanWidth,
                    child: Row(
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                                padding:
                                    EdgeInsets.only(top: 3, bottom: 3, left: 3),
                                margin: EdgeInsets.only(bottom: 2),
                                color: Colors.red[300],
                                width: max_PorWidth * 0.37,
                                // width: 150,
                                child: Text(
                                  "Advisor",
                                  style: TextStyle(
                                      fontSize: 11,
                                      fontWeight: FontWeight.w500),
                                )),
                            Container(
                                padding:
                                    EdgeInsets.only(top: 3, bottom: 3, left: 3),
                                color: Colors.red[300],
                                // width: 150,
                                width: max_PorWidth * 0.37,
                                child: Text(
                                  "Advisors assistant",
                                  style: TextStyle(
                                      fontSize: 11,
                                      fontWeight: FontWeight.w500),
                                )),
                          ],
                        ),
                        Column(
                          children: [
                            Container(
                                padding:
                                    EdgeInsets.only(top: 3, bottom: 3, left: 3),
                                margin: EdgeInsets.only(bottom: 2),
                                color: Colors.red[200],
                                width: max_PorWidth - (max_PorWidth * 0.37) - 6,
                                // width: 230,
                                child: Text("Pusit Kulkasem",
                                    style: TextStyle(
                                        fontSize: 11,
                                        fontWeight: FontWeight.w500))),
                            Container(
                                padding:
                                    EdgeInsets.only(top: 3, bottom: 3, left: 3),
                                color: Colors.red[200],
                                width: max_PorWidth - (max_PorWidth * 0.37) - 6,
                                // width: 230,
                                child: Text("Komate Amphawan",
                                    style: TextStyle(
                                        fontSize: 11,
                                        fontWeight: FontWeight.w500))),
                          ],
                        )
                      ],
                    ),
                  )),
              Row(
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 20),
                    padding: EdgeInsets.only(left: 3, top: 5, bottom: 5),
                    color: Colors.teal,
                    alignment: Alignment.center,
                    width: MediaQuery.of(context).orientation ==
                            Orientation.portrait
                        ? max_PorWidth
                        : max_LanWidth,
                    // width: 380,
                    child: Text("Exam table",
                        style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 16,
                            color: Colors.white)),
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 5.2,
                        height: max_PorWidth / 15,
                        color: Colors.teal[200],
                        child: Text(
                          "Subject code",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 5.2,
                        height: max_PorWidth / 15,
                        color: Colors.teal[200],
                        child: Text(
                          "Subject",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 5.2,
                        height: max_PorWidth / 15,
                        color: Colors.teal[200],
                        child: Text(
                          "Group",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 5.2,
                        height: max_PorWidth / 15,
                        color: Colors.teal[200],
                        child: Text(
                          "Midterm",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 5.2,
                        height: max_PorWidth / 15,
                        color: Colors.teal[200],
                        child: Text(
                          "Final",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 5.2,
                        height: max_PorWidth / 15,
                        color: Colors.teal[100],
                        child: Text(
                          "88624359-59",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 5.2,
                        height: max_PorWidth / 15,
                        color: Colors.teal[100],
                        child: Text(
                          "Web Programming",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 5.2,
                        height: max_PorWidth / 15,
                        color: Colors.teal[100],
                        child: Text(
                          "2",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 5.2,
                        height: max_PorWidth / 15,
                        color: Colors.teal[100],
                        child: Text(
                          "16-Jan-66\n17:00-20:00",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 5.2,
                        height: max_PorWidth / 15,
                        color: Colors.teal[100],
                        child: Text(
                          "27-Mar-66\n"
                          "17:00-20:00",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 5.2,
                        height: max_PorWidth / 15,
                        color: Colors.teal[200],
                        child: Text(
                          "88624459-59",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 5.2,
                        height: max_PorWidth / 15,
                        color: Colors.teal[200],
                        child: Text(
                          "OOAD",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 5.2,
                        height: max_PorWidth / 15,
                        color: Colors.teal[200],
                        child: Text(
                          "2",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 5.2,
                        height: max_PorWidth / 15,
                        color: Colors.teal[200],
                        child: Text(
                          "17-Jan-66\n17:00-20:00",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 5.2,
                        height: max_PorWidth / 15,
                        color: Colors.teal[200],
                        child: Text(
                          "28-Mar-66\n"
                          "17:00-20:00",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 5.2,
                        height: max_PorWidth / 15,
                        color: Colors.teal[100],
                        child: Text(
                          "88624559-59",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 5.2,
                        height: max_PorWidth / 15,
                        color: Colors.teal[100],
                        child: Text(
                          "Software Testing",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 5.2,
                        height: max_PorWidth / 15,
                        color: Colors.teal[100],
                        child: Text(
                          "2",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 5.2,
                        height: max_PorWidth / 15,
                        color: Colors.teal[100],
                        child: Text(
                          "16-Jan-66\n09:00-12:00",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 5.2,
                        height: max_PorWidth / 15,
                        color: Colors.teal[100],
                        child: Text(
                          "27-Mar-66\n"
                          "09:00-12:00",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 5.2,
                        height: max_PorWidth / 15,
                        color: Colors.teal[200],
                        child: Text(
                          "88634259-59",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 5.2,
                        height: max_PorWidth / 15,
                        color: Colors.teal[200],
                        child: Text(
                          "Multimedia Programming",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 5.2,
                        height: max_PorWidth / 15,
                        color: Colors.teal[200],
                        child: Text(
                          "2",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 5.2,
                        height: max_PorWidth / 15,
                        color: Colors.teal[200],
                        child: Text(
                          "18-Jan-66\n13:00-16:00",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 5.2,
                        height: max_PorWidth / 15,
                        color: Colors.teal[200],
                        child: Text(
                          "29-Mar-66\n"
                          "13:00-16:00",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 5.2,
                        height: max_PorWidth / 15,
                        color: Colors.teal[100],
                        child: Text(
                          "88634459-59",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 5.2,
                        height: max_PorWidth / 15,
                        color: Colors.teal[100],
                        child: Text(
                          "Mobile App Development",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 5.2,
                        height: max_PorWidth / 15,
                        color: Colors.teal[100],
                        child: Text(
                          "2",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 5.2,
                        height: max_PorWidth / 15,
                        color: Colors.teal[100],
                        child: Text(
                          "20-Jan-66\n13:00-16:00",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 5.2,
                        height: max_PorWidth / 15,
                        color: Colors.teal[100],
                        child: Text(
                          "29-Mar-66\n"
                          "09:00-12:00",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 5.2,
                        height: max_PorWidth / 15,
                        color: Colors.teal[200],
                        child: Text(
                          "88646259-59",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 5.2,
                        height: max_PorWidth / 15,
                        color: Colors.teal[200],
                        child: Text(
                          "NLP",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 5.2,
                        height: max_PorWidth / 15,
                        color: Colors.teal[200],
                        child: Text(
                          "2",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 5.2,
                        height: max_PorWidth / 15,
                        color: Colors.teal[200],
                        child: Text(
                          "18-Jan-66\n17:00-20:00",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 5.2,
                        height: max_PorWidth / 15,
                        color: Colors.teal[200],
                        child: Text(
                          "31-Mar-66\n"
                          "09:00-12:00",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  )
                ],
              ),
            ])));
  }
}
