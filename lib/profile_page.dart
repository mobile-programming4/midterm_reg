import 'dart:ui';

import 'package:column_widget_example/grade_page.dart';
import 'package:column_widget_example/login_page.dart';
import 'package:column_widget_example/pay_page.dart';
import 'package:column_widget_example/schedule_page.dart';
import 'package:flutter/material.dart';

class Profile extends StatelessWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final max_PorWidth = MediaQuery.of(context).size.width - 10;
    double max_LanWidth = MediaQuery.of(context).size.width - 10;

    return Scaffold(
        appBar: AppBar(
            title:
                Text("Reg BUU", style: TextStyle(fontWeight: FontWeight.bold)),
            backgroundColor: Colors.amber,
            actions: [
              IconButton(
                icon: Icon(Icons.logout_outlined),
                color: Colors.white,
                onPressed: () {
                  Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (context) {
                    return LoginPage();
                  }));
                },
              )
            ],
            toolbarHeight: 60,
            bottom: Tab(
                height: 45,
                child: Container(
                  width: double.infinity,
                  color: Colors.amberAccent,
                  child: ListView(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          IconButton(
                            onPressed: () {
                              Navigator.pushReplacement(context,
                                  MaterialPageRoute(builder: (context) {
                                return Profile();
                              }));
                            },
                            icon: Icon(Icons.people, color: Colors.black),
                          ),
                          IconButton(
                            onPressed: () {
                              Navigator.pushReplacement(context,
                                  MaterialPageRoute(builder: (context) {
                                return Schedule();
                              }));
                            },
                            icon: Icon(Icons.schedule, color: Colors.black38),
                          ),
                          IconButton(
                            onPressed: () {
                              Navigator.pushReplacement(context,
                                  MaterialPageRoute(builder: (context) {
                                return Grade();
                              }));
                            },
                            icon: Icon(Icons.grade, color: Colors.black38),
                          ),
                          IconButton(
                            onPressed: () {
                              Navigator.pushReplacement(context,
                                  MaterialPageRoute(builder: (context) {
                                return Pay();
                              }));
                            },
                            icon: Icon(Icons.qr_code_scanner,
                                color: Colors.black38),
                          ),
                        ],
                      )
                    ],
                  ),
                ))),
        body: Container(
          margin: EdgeInsets.all(5),
          constraints: BoxConstraints(
              maxWidth:
                  MediaQuery.of(context).orientation == Orientation.portrait
                      ? max_PorWidth
                      : max_LanWidth),
          // color: Colors.lightBlueAccent,
          child: ListView(
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        constraints: BoxConstraints(maxWidth: 150),
                        margin: EdgeInsets.only(right: 5, bottom: 5),
                        child: Image.network(
                          "https://www.i-pic.info/i/AZwW355462.png",
                          // width: 150,
                          width: MediaQuery.of(context).orientation ==
                                  Orientation.portrait
                              ? max_PorWidth * 0.4
                              : max_LanWidth * 0.2,
                        ),
                        color: Colors.green,
                      )
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Container(
                            margin: EdgeInsets.only(bottom: 2),
                            padding:
                                EdgeInsets.only(left: 3, top: 5, bottom: 5),
                            color: Colors.teal[200],
                            width: max_PorWidth - 150 - 5,
                            // width: 225,
                            child: Text("History Regulation",
                                style: TextStyle(
                                    fontWeight: FontWeight.w600, fontSize: 16)),
                          )
                        ],
                      ),
                      Row(
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                  padding: EdgeInsets.only(
                                      left: 3, top: 3, bottom: 3),
                                  margin: EdgeInsets.only(bottom: 2),
                                  color: Colors.black26,
                                  width: MediaQuery.of(context).orientation ==
                                          Orientation.portrait
                                      ? max_PorWidth * 0.2
                                      : max_LanWidth * 0.27,
                                  // width: 70,
                                  child: Text(
                                    "ID",
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500),
                                  )),
                              Container(
                                padding:
                                    EdgeInsets.only(left: 3, top: 3, bottom: 3),
                                margin: EdgeInsets.only(bottom: 2),
                                color: Colors.black26,
                                width: MediaQuery.of(context).orientation ==
                                        Orientation.portrait
                                    ? max_PorWidth * 0.2
                                    : max_LanWidth * 0.27,
                                // width: 70,
                                child: Text("Name",
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500)),
                              ),
                              Container(
                                  padding: EdgeInsets.only(
                                      left: 3, top: 3, bottom: 3),
                                  margin: EdgeInsets.only(bottom: 2),
                                  color: Colors.black26,
                                  width: MediaQuery.of(context).orientation ==
                                          Orientation.portrait
                                      ? max_PorWidth * 0.2
                                      : max_LanWidth * 0.27,
                                  // width: 70,
                                  child: Text(
                                    "Faculty",
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500),
                                  )),
                              Container(
                                  padding: EdgeInsets.only(
                                      left: 3, top: 3, bottom: 3),
                                  margin: EdgeInsets.only(bottom: 2),
                                  color: Colors.black26,
                                  width: MediaQuery.of(context).orientation ==
                                          Orientation.portrait
                                      ? max_PorWidth * 0.2
                                      : max_LanWidth * 0.27,
                                  // width: 70,
                                  child: Text(
                                    "Campus",
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500),
                                  )),
                              Container(
                                  padding: EdgeInsets.only(
                                      left: 3, top: 3, bottom: 3),
                                  margin: EdgeInsets.only(bottom: 2),
                                  color: Colors.black26,
                                  width: MediaQuery.of(context).orientation ==
                                          Orientation.portrait
                                      ? max_PorWidth * 0.2
                                      : max_LanWidth * 0.27,
                                  // width: 70,
                                  child: Text(
                                    "Education",
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500),
                                  )),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                  padding: EdgeInsets.only(
                                      left: 3, top: 3, bottom: 3),
                                  margin: EdgeInsets.only(bottom: 2),
                                  color: Colors.black12,
                                  width: MediaQuery.of(context).orientation ==
                                          Orientation.portrait
                                      ? max_PorWidth -
                                          (max_PorWidth * 0.2) -
                                          150 -
                                          5
                                      : max_LanWidth -
                                          (max_LanWidth * 0.27) -
                                          150 -
                                          5,
                                  // width: 155,
                                  child: Text("63160011",
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w500))),
                              Container(
                                padding:
                                    EdgeInsets.only(left: 3, top: 3, bottom: 3),
                                margin: EdgeInsets.only(bottom: 2),
                                color: Colors.black12,
                                width: MediaQuery.of(context).orientation ==
                                        Orientation.portrait
                                    ? max_PorWidth -
                                        (max_PorWidth * 0.2) -
                                        150 -
                                        5
                                    : max_LanWidth -
                                        (max_LanWidth * 0.27) -
                                        150 -
                                        5,
                                // width: 155,
                                child: Text("Waritphat Kheereerat",
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500)),
                              ),
                              Container(
                                  padding: EdgeInsets.only(
                                      left: 3, top: 3, bottom: 3),
                                  margin: EdgeInsets.only(bottom: 2),
                                  color: Colors.black12,
                                  // width: 155,
                                  width: MediaQuery.of(context).orientation ==
                                          Orientation.portrait
                                      ? max_PorWidth -
                                          (max_PorWidth * 0.2) -
                                          150 -
                                          5
                                      : max_LanWidth -
                                          (max_LanWidth * 0.27) -
                                          150 -
                                          5,
                                  child: Text("Informatic",
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w500))),
                              Container(
                                  padding: EdgeInsets.only(
                                      left: 3, top: 3, bottom: 3),
                                  margin: EdgeInsets.only(bottom: 2),
                                  color: Colors.black12,
                                  // width: 155,
                                  width: MediaQuery.of(context).orientation ==
                                          Orientation.portrait
                                      ? max_PorWidth -
                                          (max_PorWidth * 0.2) -
                                          150 -
                                          5
                                      : max_LanWidth -
                                          (max_LanWidth * 0.27) -
                                          150 -
                                          5,
                                  child: Text("Bangsaen",
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w500))),
                              Container(
                                  padding: EdgeInsets.only(
                                      left: 3, top: 3, bottom: 3),
                                  margin: EdgeInsets.only(bottom: 2),
                                  color: Colors.black12,
                                  // width: 155,
                                  width: MediaQuery.of(context).orientation ==
                                          Orientation.portrait
                                      ? max_PorWidth -
                                          (max_PorWidth * 0.2) -
                                          150 -
                                          5
                                      : max_LanWidth -
                                          (max_LanWidth * 0.27) -
                                          150 -
                                          5,
                                  child: Text("Bachelor Degrees",
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w500))),
                            ],
                          )
                        ],
                      )
                    ],
                  )
                ],
              ),
              Row(
                children: [
                  Container(
                    margin: EdgeInsets.only(bottom: 2),
                    padding: EdgeInsets.only(left: 3, top: 5, bottom: 5),
                    color: Colors.teal[200],
                    width: MediaQuery.of(context).orientation ==
                            Orientation.portrait
                        ? max_PorWidth
                        : max_LanWidth,
                    // width: 380,
                    child: Text("Grades",
                        style: TextStyle(
                            fontWeight: FontWeight.w600, fontSize: 16)),
                  )
                ],
              ),
              Row(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                          padding: EdgeInsets.only(left: 3, top: 3, bottom: 3),
                          margin: EdgeInsets.only(bottom: 2),
                          color: Colors.black26,
                          width: MediaQuery.of(context).orientation ==
                                  Orientation.portrait
                              ? max_PorWidth * 0.37
                              : max_LanWidth * 0.37,
                          // width: 150,
                          child: Text(
                            "All-Credit",
                            style: TextStyle(
                                fontSize: 14, fontWeight: FontWeight.w500),
                          )),
                      Container(
                          padding: EdgeInsets.only(left: 3, top: 3, bottom: 3),
                          margin: EdgeInsets.only(bottom: 2),
                          color: Colors.black26,
                          // width: 150,
                          width: MediaQuery.of(context).orientation ==
                                  Orientation.portrait
                              ? max_PorWidth * 0.37
                              : max_LanWidth * 0.37,
                          child: Text(
                            "Passed-Credit",
                            style: TextStyle(
                                fontSize: 14, fontWeight: FontWeight.w500),
                          )),
                      Container(
                          padding: EdgeInsets.only(left: 3, top: 3, bottom: 3),
                          margin: EdgeInsets.only(bottom: 2),
                          color: Colors.black26,
                          width: MediaQuery.of(context).orientation ==
                                  Orientation.portrait
                              ? max_PorWidth * 0.37
                              : max_LanWidth * 0.37,
                          // width: 150,
                          child: Text(
                            "GPA",
                            style: TextStyle(
                                fontSize: 14, fontWeight: FontWeight.w500),
                          )),
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                          padding: EdgeInsets.only(left: 3, top: 3, bottom: 3),
                          margin: EdgeInsets.only(bottom: 2),
                          color: Colors.black12,
                          width: MediaQuery.of(context).orientation ==
                                  Orientation.portrait
                              ? max_PorWidth - (max_PorWidth * 0.37)
                              : max_LanWidth - (max_LanWidth * 0.37),
                          // width: 230,
                          child: Text("93",
                              style: TextStyle(
                                  fontSize: 14, fontWeight: FontWeight.w500))),
                      Container(
                          padding: EdgeInsets.only(left: 3, top: 3, bottom: 3),
                          margin: EdgeInsets.only(bottom: 2),
                          color: Colors.black12,
                          width: MediaQuery.of(context).orientation ==
                                  Orientation.portrait
                              ? max_PorWidth - (max_PorWidth * 0.37)
                              : max_LanWidth - (max_LanWidth * 0.37),
                          // width: 230,
                          child: Text("93",
                              style: TextStyle(
                                  fontSize: 14, fontWeight: FontWeight.w500))),
                      Container(
                          padding: EdgeInsets.only(left: 3, top: 3, bottom: 3),
                          margin: EdgeInsets.only(bottom: 2),
                          color: Colors.black12,
                          width: MediaQuery.of(context).orientation ==
                                  Orientation.portrait
                              ? max_PorWidth - (max_PorWidth * 0.37)
                              : max_LanWidth - (max_LanWidth * 0.37),
                          // width: 230,
                          child: Text("3.43",
                              style: TextStyle(
                                  fontSize: 14, fontWeight: FontWeight.w500))),
                    ],
                  )
                ],
              ),
              Row(
                children: [
                  Container(
                    margin: EdgeInsets.only(bottom: 2),
                    padding: EdgeInsets.only(left: 3, top: 5, bottom: 5),
                    color: Colors.teal[200],
                    width: MediaQuery.of(context).orientation ==
                            Orientation.portrait
                        ? max_PorWidth
                        : max_LanWidth,
                    // width: 380,
                    child: Text("Personal information",
                        style: TextStyle(
                            fontWeight: FontWeight.w600, fontSize: 16)),
                  )
                ],
              ),
              Row(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                          padding: EdgeInsets.only(left: 3, top: 3, bottom: 3),
                          margin: EdgeInsets.only(bottom: 2),
                          color: Colors.black26,
                          width: MediaQuery.of(context).orientation ==
                                  Orientation.portrait
                              ? max_PorWidth * 0.37
                              : max_LanWidth * 0.37,
                          // width: 150,
                          child: Text(
                            "Nationality",
                            style: TextStyle(
                                fontSize: 14, fontWeight: FontWeight.w500),
                          )),
                      Container(
                          padding: EdgeInsets.only(left: 3, top: 3, bottom: 3),
                          margin: EdgeInsets.only(bottom: 2),
                          color: Colors.black26,
                          // width: 150,
                          width: MediaQuery.of(context).orientation ==
                                  Orientation.portrait
                              ? max_PorWidth * 0.37
                              : max_LanWidth * 0.37,
                          child: Text(
                            "Religion",
                            style: TextStyle(
                                fontSize: 14, fontWeight: FontWeight.w500),
                          )),
                      Container(
                          padding: EdgeInsets.only(left: 3, top: 3, bottom: 3),
                          margin: EdgeInsets.only(bottom: 2),
                          color: Colors.black26,
                          width: MediaQuery.of(context).orientation ==
                                  Orientation.portrait
                              ? max_PorWidth * 0.37
                              : max_LanWidth * 0.37,
                          // width: 150,
                          child: Text(
                            "Blood type",
                            style: TextStyle(
                                fontSize: 14, fontWeight: FontWeight.w500),
                          )),
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                          padding: EdgeInsets.only(left: 3, top: 3, bottom: 3),
                          margin: EdgeInsets.only(bottom: 2),
                          color: Colors.black12,
                          width: MediaQuery.of(context).orientation ==
                                  Orientation.portrait
                              ? max_PorWidth - (max_PorWidth * 0.37)
                              : max_LanWidth - (max_LanWidth * 0.37),
                          // width: 230,
                          child: Text("Thai",
                              style: TextStyle(
                                  fontSize: 14, fontWeight: FontWeight.w500))),
                      Container(
                          padding: EdgeInsets.only(left: 3, top: 3, bottom: 3),
                          margin: EdgeInsets.only(bottom: 2),
                          color: Colors.black12,
                          width: MediaQuery.of(context).orientation ==
                                  Orientation.portrait
                              ? max_PorWidth - (max_PorWidth * 0.37)
                              : max_LanWidth - (max_LanWidth * 0.37),
                          // width: 230,
                          child: Text("Buddhism",
                              style: TextStyle(
                                  fontSize: 14, fontWeight: FontWeight.w500))),
                      Container(
                          padding: EdgeInsets.only(left: 3, top: 3, bottom: 3),
                          margin: EdgeInsets.only(bottom: 2),
                          color: Colors.black12,
                          width: MediaQuery.of(context).orientation ==
                                  Orientation.portrait
                              ? max_PorWidth - (max_PorWidth * 0.37)
                              : max_LanWidth - (max_LanWidth * 0.37),
                          // width: 230,
                          child: Text("O",
                              style: TextStyle(
                                  fontSize: 14, fontWeight: FontWeight.w500))),
                    ],
                  )
                ],
              ),
              Row(
                children: [
                  Container(
                    margin: EdgeInsets.only(bottom: 2),
                    padding: EdgeInsets.only(left: 3, top: 5, bottom: 5),
                    color: Colors.teal[200],
                    width: MediaQuery.of(context).orientation ==
                        Orientation.portrait
                        ? max_PorWidth
                        : max_LanWidth,
                    // width: 380,
                    child: Text("History of university",
                        style: TextStyle(
                            fontWeight: FontWeight.w600, fontSize: 16)),
                  )
                ],
              ),
              Row(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                          padding: EdgeInsets.only(left: 3, top: 3, bottom: 3),
                          margin: EdgeInsets.only(bottom: 2),
                          color: Colors.black26,
                          width: MediaQuery.of(context).orientation ==
                              Orientation.portrait
                              ? max_PorWidth * 0.37
                              : max_LanWidth * 0.37,
                          // width: 150,
                          child: Text(
                            "Date",
                            style: TextStyle(
                                fontSize: 14, fontWeight: FontWeight.w500),
                          )),
                      Container(
                          padding: EdgeInsets.only(left: 3, top: 3, bottom: 3),
                          margin: EdgeInsets.only(bottom: 2),
                          color: Colors.black26,
                          // width: 150,
                          width: MediaQuery.of(context).orientation ==
                              Orientation.portrait
                              ? max_PorWidth * 0.37
                              : max_LanWidth * 0.37,
                          child: Text(
                            "Term",
                            style: TextStyle(
                                fontSize: 14, fontWeight: FontWeight.w500),
                          )),
                      Container(
                          padding: EdgeInsets.only(left: 3, top: 3, bottom: 3),
                          margin: EdgeInsets.only(bottom: 2),
                          color: Colors.black26,
                          width: MediaQuery.of(context).orientation ==
                              Orientation.portrait
                              ? max_PorWidth * 0.37
                              : max_LanWidth * 0.37,
                          // width: 150,
                          child: Text(
                            "History",
                            style: TextStyle(
                                fontSize: 14, fontWeight: FontWeight.w500),
                          )),
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                          padding: EdgeInsets.only(left: 3, top: 3, bottom: 3),
                          margin: EdgeInsets.only(bottom: 2),
                          color: Colors.black12,
                          width: MediaQuery.of(context).orientation ==
                              Orientation.portrait
                              ? max_PorWidth - (max_PorWidth * 0.37)
                              : max_LanWidth - (max_LanWidth * 0.37),
                          // width: 230,
                          child: Text("19/08/2021",
                              style: TextStyle(
                                  fontSize: 14, fontWeight: FontWeight.w500))),
                      Container(
                          padding: EdgeInsets.only(left: 3, top: 3, bottom: 3),
                          margin: EdgeInsets.only(bottom: 2),
                          color: Colors.black12,
                          width: MediaQuery.of(context).orientation ==
                              Orientation.portrait
                              ? max_PorWidth - (max_PorWidth * 0.37)
                              : max_LanWidth - (max_LanWidth * 0.37),
                          // width: 230,
                          child: Text("2564/1",
                              style: TextStyle(
                                  fontSize: 14, fontWeight: FontWeight.w500))),
                      Container(
                          padding: EdgeInsets.only(left: 3, top: 3, bottom: 3),
                          margin: EdgeInsets.only(bottom: 2),
                          color: Colors.black12,
                          width: MediaQuery.of(context).orientation ==
                              Orientation.portrait
                              ? max_PorWidth - (max_PorWidth * 0.37)
                              : max_LanWidth - (max_LanWidth * 0.37),
                          // width: 230,
                          child: Text("0",
                              style: TextStyle(
                                  fontSize: 14, fontWeight: FontWeight.w500))),
                    ],
                  )
                ],
              ),
            ],
          ),
        ));
  }
}
