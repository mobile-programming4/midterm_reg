import 'package:column_widget_example/main_page.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: null,
        body: Center(
          child: ConstrainedBox(
            constraints: const BoxConstraints(maxWidth: 300.0),
            child: Column(
              // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                //const SizedBox(height: 50.0),
                const Spacer(flex: 5),
                Image.network(
                  "https://upload.wikimedia.org/wikipedia/commons/e/ec/Buu-logo11.png",
                  width: 150,
                ),
                //const SizedBox(height: 100.0),
                const Spacer(flex: 7),
                const TextField(
                    decoration: InputDecoration(labelText: 'Username')),
                const TextField(
                    decoration: InputDecoration(labelText: 'Password')),
                const Spacer(flex: 5),
                //const SizedBox(height: 30.0),

                ElevatedButton(
                    onPressed: () {
                      Navigator.pushReplacement(context,
                          MaterialPageRoute(builder: (context) {
                        return MainPage();
                      }));
                    },
                    child: const Text('Login')),
                const Spacer(flex: 20),
              ],
            ),
          ),
        ));
  }
}
