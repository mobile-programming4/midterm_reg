import 'package:column_widget_example/grade_page.dart';
import 'package:column_widget_example/login_page.dart';
import 'package:column_widget_example/profile_page.dart';
import 'package:column_widget_example/schedule_page.dart';
import 'package:flutter/material.dart';

class Pay extends StatelessWidget {
  const Pay({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final max_PorWidth = MediaQuery.of(context).size.width - 10;
    double max_LanWidth = MediaQuery.of(context).size.width - 10;
    return Scaffold(
        appBar: AppBar(
            title:
                Text("Reg BUU", style: TextStyle(fontWeight: FontWeight.bold)),
            backgroundColor: Colors.amber,
            actions: [
              IconButton(
                icon: Icon(Icons.logout_outlined),
                color: Colors.white,
                onPressed: () {
                  Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (context) {
                    return LoginPage();
                  }));
                },
              )
            ],
            toolbarHeight: 60,
            bottom: Tab(
                height: 45,
                child: Container(
                  width: double.infinity,
                  color: Colors.amberAccent,
                  child: ListView(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          IconButton(
                            onPressed: () {
                              Navigator.pushReplacement(context,
                                  MaterialPageRoute(builder: (context) {
                                return Profile();
                              }));
                            },
                            icon: Icon(Icons.people, color: Colors.black38),
                          ),
                          IconButton(
                            onPressed: () {
                              Navigator.pushReplacement(context,
                                  MaterialPageRoute(builder: (context) {
                                return Schedule();
                              }));
                            },
                            icon: Icon(Icons.schedule, color: Colors.black38),
                          ),
                          IconButton(
                            onPressed: () {
                              Navigator.pushReplacement(context,
                                  MaterialPageRoute(builder: (context) {
                                return Grade();
                              }));
                            },
                            icon: Icon(Icons.grade, color: Colors.black38),
                          ),
                          IconButton(
                            onPressed: () {
                              Navigator.pushReplacement(context,
                                  MaterialPageRoute(builder: (context) {
                                return Pay();
                              }));
                            },
                            icon: Icon(Icons.qr_code_scanner,
                                color: Colors.black),
                          ),
                        ],
                      )
                    ],
                  ),
                ))),
        body: Container(
            alignment: Alignment.topCenter,
            margin: EdgeInsets.all(5),
            constraints: BoxConstraints(
                maxWidth:
                    MediaQuery.of(context).orientation == Orientation.portrait
                        ? max_PorWidth
                        : max_LanWidth),
            // color: Colors.lightBlueAccent,
            child: Container(
              margin: EdgeInsets.only(top: 10, left: 10, right: 10),
              color: Colors.black12,
              width: max_LanWidth,
              height: max_LanWidth / 1,
              child: ListView(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                          margin: EdgeInsets.only(top: 10, left: 10),
                          child: Image.network(
                            "https://www.i-pic.info/i/o4qW357590.png",
                            width: max_LanWidth * 0.25,
                          )),
                      IconButton(onPressed: () {}, icon: Icon(Icons.refresh))
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                          margin: EdgeInsets.only(),
                          child: Image.network(
                            "https://www.i-pic.info/i/nLGo357593.png",
                            width: max_LanWidth * 0.8,
                          ))
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                          child: Image.network(
                        "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d0/QR_code_for_mobile_English_Wikipedia.svg/1200px-QR_code_for_mobile_English_Wikipedia.svg.png",
                        width: max_LanWidth * 0.3,
                      ))
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                          color: Colors.black12,
                          padding: EdgeInsets.only(
                              top: 5, bottom: 5, left: 10, right: 10),
                          width: max_LanWidth * 0.6,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Image.network(
                                "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b7/MasterCard_Logo.svg/2560px-MasterCard_Logo.svg.png",
                                width: max_LanWidth * 0.1,
                              ),
                              Text(
                                "Credit/ debit card\n*1709",
                                style: TextStyle(fontSize: 12),
                              ),
                              Text(
                                "Change",
                                style: TextStyle(
                                    fontSize: 12, color: Colors.orange),
                              ),
                            ],
                          ))
                    ],
                  ),
                ],
              ),
            )));
  }
}
