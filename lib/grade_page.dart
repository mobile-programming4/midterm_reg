import 'package:column_widget_example/login_page.dart';
import 'package:column_widget_example/pay_page.dart';
import 'package:column_widget_example/profile_page.dart';
import 'package:column_widget_example/schedule_page.dart';
import 'package:flutter/material.dart';

class Grade extends StatelessWidget {
  const Grade({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final max_PorWidth = MediaQuery.of(context).size.width - 10;
    double max_LanWidth = MediaQuery.of(context).size.width - 10;
    return Scaffold(
        appBar: AppBar(
            title:
                Text("Reg BUU", style: TextStyle(fontWeight: FontWeight.bold)),
            backgroundColor: Colors.amber,
            actions: [
              IconButton(
                icon: Icon(Icons.logout_outlined),
                color: Colors.white,
                onPressed: () {
                  Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (context) {
                    return LoginPage();
                  }));
                },
              )
            ],
            toolbarHeight: 60,
            bottom: Tab(
                height: 45,
                child: Container(
                  width: double.infinity,
                  color: Colors.amberAccent,
                  child: ListView(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          IconButton(
                            onPressed: () {
                              Navigator.pushReplacement(context,
                                  MaterialPageRoute(builder: (context) {
                                return Profile();
                              }));
                            },
                            icon: Icon(Icons.people, color: Colors.black38),
                          ),
                          IconButton(
                            onPressed: () {
                              Navigator.pushReplacement(context,
                                  MaterialPageRoute(builder: (context) {
                                return Schedule();
                              }));
                            },
                            icon: Icon(Icons.schedule, color: Colors.black38),
                          ),
                          IconButton(
                            onPressed: () {
                              Navigator.pushReplacement(context,
                                  MaterialPageRoute(builder: (context) {
                                return Grade();
                              }));
                            },
                            icon: Icon(Icons.grade, color: Colors.black),
                          ),
                          IconButton(
                            onPressed: () {
                              Navigator.pushReplacement(context,
                                  MaterialPageRoute(builder: (context) {
                                return Pay();
                              }));
                            },
                            icon: Icon(Icons.qr_code_scanner,
                                color: Colors.black38),
                          ),
                        ],
                      )
                    ],
                  ),
                ))),
        body: Container(
            margin: EdgeInsets.all(5),
            constraints: BoxConstraints(
                maxWidth:
                    MediaQuery.of(context).orientation == Orientation.portrait
                        ? max_PorWidth
                        : max_LanWidth),
            // color: Colors.lightBlueAccent,
            child: ListView(children: [
              Row(
                children: [
                  Container(
                    padding: EdgeInsets.only(left: 3, top: 5, bottom: 5),
                    color: Colors.indigo,
                    alignment: Alignment.center,
                    width: MediaQuery.of(context).orientation ==
                            Orientation.portrait
                        ? max_PorWidth
                        : max_LanWidth,
                    // width: 380,
                    child: Text("Year 2020/1",
                        style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 16,
                            color: Colors.white)),
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 5.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[200],
                        child: Text(
                          "Subject code",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 4 * 2,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[200],
                        child: Text(
                          "Subject",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[200],
                        child: Text(
                          "Credit",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[200],
                        child: Text(
                          "Grade",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 5.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "30910159",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 4 * 2,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "Marine Ecology and Ecotourism",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "2",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "A",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 5.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "40240359",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 4 * 2,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "Sufficiency Economy and Social Development",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "2",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "B+",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 5.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "85111059",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 4 * 2,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "Exercise for Quality of Life",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "2",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "A",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 5.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "88510059",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 4 * 2,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "Logical Thinking and Problem Solving for Innovation",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "2",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "A",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 5.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "88510159",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 4 * 2,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "Moving Forward in a Digital Society with ICT",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "3",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "A",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 5.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "88510259",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 4 * 2,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "Discrete Structures",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "3",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "A",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 5.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "99910259",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 4 * 2,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "Collegiate English",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "3",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "A",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  )
                ],
              ),
              Row(
                children: [
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: max_LanWidth / 2,
                        color: Colors.black54,
                        height: max_PorWidth / 15,
                        child: Text(
                          "This Semester",
                          style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: max_LanWidth / 2,
                        color: Colors.black45,
                        height: max_PorWidth / 15,
                        child: Text(
                          "Cumulative to this semester",
                          style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                ],
              ),
              Row(
                children: [
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: (max_LanWidth / 2) / 3.5,
                        color: Colors.black38,
                        height: max_PorWidth / 15,
                        child: Text(
                          "C.Register\n17",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: (max_LanWidth / 2) / 4.7,
                        color: Colors.black26,
                        height: max_PorWidth / 15,
                        child: Text(
                          "C.Earn\n17",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: (max_LanWidth / 2) / 6,
                        color: Colors.black38,
                        height: max_PorWidth / 15,
                        child: Text(
                          "CA\n17",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: (max_LanWidth / 2) / 6,
                        color: Colors.black26,
                        height: max_PorWidth / 15,
                        child: Text(
                          "GP\n67",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: (max_LanWidth / 2) / 6,
                        color: Colors.black38,
                        height: max_PorWidth / 15,
                        child: Text(
                          "GPA\n3.94",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: (max_LanWidth / 2) / 3.5,
                        color: Colors.black26,
                        height: max_PorWidth / 15,
                        child: Text(
                          "C.Register\n17",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: (max_LanWidth / 2) / 4.7,
                        color: Colors.black12,
                        height: max_PorWidth / 15,
                        child: Text(
                          "C.Earn\n17",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: (max_LanWidth / 2) / 6,
                        color: Colors.black26,
                        height: max_PorWidth / 15,
                        child: Text(
                          "CA\n17",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: (max_LanWidth / 2) / 6,
                        color: Colors.black12,
                        height: max_PorWidth / 15,
                        child: Text(
                          "GP\n67",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: (max_LanWidth / 2) / 6,
                        color: Colors.black26,
                        height: max_PorWidth / 15,
                        child: Text(
                          "GPA\n3.94",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                ],
              ),
              Row(
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 20),
                    padding: EdgeInsets.only(left: 3, top: 5, bottom: 5),
                    color: Colors.indigo,
                    alignment: Alignment.center,
                    width: MediaQuery.of(context).orientation ==
                            Orientation.portrait
                        ? max_PorWidth
                        : max_LanWidth,
                    // width: 380,
                    child: Text("Year 2020/2",
                        style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 16,
                            color: Colors.white)),
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 5.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[200],
                        child: Text(
                          "Subject code",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 4 * 2,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[200],
                        child: Text(
                          "Subject",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[200],
                        child: Text(
                          "Credit",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[200],
                        child: Text(
                          "Grade",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 5.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "61010159",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 4 * 2,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "Art and Life",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "3",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "B+",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 5.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "77037959",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 4 * 2,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "Arts and Creativity",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "2",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "A",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 5.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "88510359",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 4 * 2,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "Mathematics for Computing",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "3",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "A",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 5.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "88510459",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 4 * 2,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "Programming Fundamental",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "3",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "C+",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 5.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "88612159",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 4 * 2,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "Introduction to Computer Science",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "3",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "C+",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 5.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "99920159",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 4 * 2,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "English Writing for Communication",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "3",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "B",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  )
                ],
              ),
              Row(
                children: [
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: max_LanWidth / 2,
                        color: Colors.black54,
                        height: max_PorWidth / 15,
                        child: Text(
                          "This Semester",
                          style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: max_LanWidth / 2,
                        color: Colors.black45,
                        height: max_PorWidth / 15,
                        child: Text(
                          "Cumulative to this semester",
                          style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                ],
              ),
              Row(
                children: [
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: (max_LanWidth / 2) / 3.5,
                        color: Colors.black38,
                        height: max_PorWidth / 15,
                        child: Text(
                          "C.Register\n17",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: (max_LanWidth / 2) / 4.7,
                        color: Colors.black26,
                        height: max_PorWidth / 15,
                        child: Text(
                          "C.Earn\n17",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: (max_LanWidth / 2) / 6,
                        color: Colors.black38,
                        height: max_PorWidth / 15,
                        child: Text(
                          "CA\n17",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: (max_LanWidth / 2) / 6,
                        color: Colors.black26,
                        height: max_PorWidth / 15,
                        child: Text(
                          "GP\n54.5",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: (max_LanWidth / 2) / 6,
                        color: Colors.black38,
                        height: max_PorWidth / 15,
                        child: Text(
                          "GPA\n3.21",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: (max_LanWidth / 2) / 3.5,
                        color: Colors.black26,
                        height: max_PorWidth / 15,
                        child: Text(
                          "C.Register\n34",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: (max_LanWidth / 2) / 4.7,
                        color: Colors.black12,
                        height: max_PorWidth / 15,
                        child: Text(
                          "C.Earn\n34",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: (max_LanWidth / 2) / 6,
                        color: Colors.black26,
                        height: max_PorWidth / 15,
                        child: Text(
                          "CA\n34",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: (max_LanWidth / 2) / 6,
                        color: Colors.black12,
                        height: max_PorWidth / 15,
                        child: Text(
                          "GP\n121.5",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: (max_LanWidth / 2) / 6,
                        color: Colors.black26,
                        height: max_PorWidth / 15,
                        child: Text(
                          "GPA\n3.57",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                ],
              ),
              Row(
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 20),
                    padding: EdgeInsets.only(left: 3, top: 5, bottom: 5),
                    color: Colors.indigo,
                    alignment: Alignment.center,
                    width: MediaQuery.of(context).orientation ==
                            Orientation.portrait
                        ? max_PorWidth
                        : max_LanWidth,
                    // width: 380,
                    child: Text("Year 2021/1",
                        style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 16,
                            color: Colors.white)),
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 5.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[200],
                        child: Text(
                          "Subject code",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 4 * 2,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[200],
                        child: Text(
                          "Subject",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[200],
                        child: Text(
                          "Credit",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[200],
                        child: Text(
                          "Grade",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 5.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "25710259",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 4 * 2,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "Economics of Everyday Life",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "2",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "B+",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 5.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "30211359",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 4 * 2,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "Calculus",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "3",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "A",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 5.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "88520159",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 4 * 2,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "Probability and Statistics for Computing",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "3",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "A",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 5.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "88520259",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 4 * 2,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "English for Informatics",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "3",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "B",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 5.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "88620159",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 4 * 2,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "Object-Oriented Programming Paradigm",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "3",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "B+",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 5.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "88620259",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 4 * 2,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "Relational Database",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "3",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "B",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 5.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "99910159",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 4 * 2,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "English for Communication",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "3",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "A",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  )
                ],
              ),
              Row(
                children: [
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: max_LanWidth / 2,
                        color: Colors.black54,
                        height: max_PorWidth / 15,
                        child: Text(
                          "This Semester",
                          style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: max_LanWidth / 2,
                        color: Colors.black45,
                        height: max_PorWidth / 15,
                        child: Text(
                          "Cumulative to this semester",
                          style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                ],
              ),
              Row(
                children: [
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: (max_LanWidth / 2) / 3.5,
                        color: Colors.black38,
                        height: max_PorWidth / 15,
                        child: Text(
                          "C.Register\n20",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: (max_LanWidth / 2) / 4.7,
                        color: Colors.black26,
                        height: max_PorWidth / 15,
                        child: Text(
                          "C.Earn\n20",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: (max_LanWidth / 2) / 6,
                        color: Colors.black38,
                        height: max_PorWidth / 15,
                        child: Text(
                          "CA\n20",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: (max_LanWidth / 2) / 6,
                        color: Colors.black26,
                        height: max_PorWidth / 15,
                        child: Text(
                          "GP\n71.5",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: (max_LanWidth / 2) / 6,
                        color: Colors.black38,
                        height: max_PorWidth / 15,
                        child: Text(
                          "GPA\n3.58",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: (max_LanWidth / 2) / 3.5,
                        color: Colors.black26,
                        height: max_PorWidth / 15,
                        child: Text(
                          "C.Register\n54",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: (max_LanWidth / 2) / 4.7,
                        color: Colors.black12,
                        height: max_PorWidth / 15,
                        child: Text(
                          "C.Earn\n54",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: (max_LanWidth / 2) / 6,
                        color: Colors.black26,
                        height: max_PorWidth / 15,
                        child: Text(
                          "CA\n54",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: (max_LanWidth / 2) / 6,
                        color: Colors.black12,
                        height: max_PorWidth / 15,
                        child: Text(
                          "GP\n193",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: (max_LanWidth / 2) / 6,
                        color: Colors.black26,
                        height: max_PorWidth / 15,
                        child: Text(
                          "GPA\n3.57",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                ],
              ),
              Row(
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 20),
                    padding: EdgeInsets.only(left: 3, top: 5, bottom: 5),
                    color: Colors.indigo,
                    alignment: Alignment.center,
                    width: MediaQuery.of(context).orientation ==
                            Orientation.portrait
                        ? max_PorWidth
                        : max_LanWidth,
                    // width: 380,
                    child: Text("Year 2021/2",
                        style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 16,
                            color: Colors.white)),
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 5.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[200],
                        child: Text(
                          "Subject code",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 4 * 2,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[200],
                        child: Text(
                          "Subject",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[200],
                        child: Text(
                          "Credit",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[200],
                        child: Text(
                          "Grade",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 5.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "77920564",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 4 * 2,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "Music Appreciation",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "3",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "A",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 5.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "88620359",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 4 * 2,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "Non-Relational Database",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "3",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "B",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 5.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "88620459",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 4 * 2,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "Introduction to Data Science and Data Analytics",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "3",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "C+",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 5.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "88621159",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 4 * 2,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "Data Structures and Algorithms",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "3",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "A",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 5.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "88622259",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 4 * 2,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "Operating Systems",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "3",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "C",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 5.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "88624159",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 4 * 2,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "Unix Tools and Programming",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "3",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "B+",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  )
                ],
              ),
              Row(
                children: [
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: max_LanWidth / 2,
                        color: Colors.black54,
                        height: max_PorWidth / 15,
                        child: Text(
                          "This Semester",
                          style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: max_LanWidth / 2,
                        color: Colors.black45,
                        height: max_PorWidth / 15,
                        child: Text(
                          "Cumulative to this semester",
                          style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                ],
              ),
              Row(
                children: [
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: (max_LanWidth / 2) / 3.5,
                        color: Colors.black38,
                        height: max_PorWidth / 15,
                        child: Text(
                          "C.Register\n18",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: (max_LanWidth / 2) / 4.7,
                        color: Colors.black26,
                        height: max_PorWidth / 15,
                        child: Text(
                          "C.Earn\n18",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: (max_LanWidth / 2) / 6,
                        color: Colors.black38,
                        height: max_PorWidth / 15,
                        child: Text(
                          "CA\n18",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: (max_LanWidth / 2) / 6,
                        color: Colors.black26,
                        height: max_PorWidth / 15,
                        child: Text(
                          "GP\n57",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: (max_LanWidth / 2) / 6,
                        color: Colors.black38,
                        height: max_PorWidth / 15,
                        child: Text(
                          "GPA\n3.17",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: (max_LanWidth / 2) / 3.5,
                        color: Colors.black26,
                        height: max_PorWidth / 15,
                        child: Text(
                          "C.Register\n72",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: (max_LanWidth / 2) / 4.7,
                        color: Colors.black12,
                        height: max_PorWidth / 15,
                        child: Text(
                          "C.Earn\n72",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: (max_LanWidth / 2) / 6,
                        color: Colors.black26,
                        height: max_PorWidth / 15,
                        child: Text(
                          "CA\n72",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: (max_LanWidth / 2) / 6,
                        color: Colors.black12,
                        height: max_PorWidth / 15,
                        child: Text(
                          "GP\n250",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: (max_LanWidth / 2) / 6,
                        color: Colors.black26,
                        height: max_PorWidth / 15,
                        child: Text(
                          "GPA\n3.47",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                ],
              ),
              Row(
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 20),
                    padding: EdgeInsets.only(left: 3, top: 5, bottom: 5),
                    color: Colors.indigo,
                    alignment: Alignment.center,
                    width: MediaQuery.of(context).orientation ==
                            Orientation.portrait
                        ? max_PorWidth
                        : max_LanWidth,
                    // width: 380,
                    child: Text("Year 2022/1",
                        style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 16,
                            color: Colors.white)),
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 5.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[200],
                        child: Text(
                          "Subject code",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 4 * 2,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[200],
                        child: Text(
                          "Subject",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[200],
                        child: Text(
                          "Credit",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[200],
                        child: Text(
                          "Grade",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 5.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "77920364",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 4 * 2,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "Thai Music Culture in Life",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "3",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "A",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 5.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "88624259",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 4 * 2,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "Mobile Programming Paradigm",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "3",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "B+",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 5.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "88631159",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 4 * 2,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "Algorithm Design and Applications",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "3",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "B+",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 5.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "88633159",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 4 * 2,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "Computer Networks",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "3",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "B",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 5.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "88634159",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 4 * 2,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "Software Development",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "3",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "B",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 5.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "88635359",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 4 * 2,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "User Interface Design and Development",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "3",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "B+",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 5.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "88636159",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        padding: EdgeInsets.only(left: 3),
                        alignment: Alignment.centerLeft,
                        width: max_PorWidth / 4 * 2,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "Introduction to Artificial Intelligence",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "3",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 1),
                        alignment: Alignment.center,
                        width: max_PorWidth / 7.1,
                        height: max_PorWidth / 15,
                        color: Colors.indigo[100],
                        child: Text(
                          "C+",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  )
                ],
              ),
              Row(
                children: [
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: max_LanWidth / 2,
                        color: Colors.black54,
                        height: max_PorWidth / 15,
                        child: Text(
                          "This Semester",
                          style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: max_LanWidth / 2,
                        color: Colors.black45,
                        height: max_PorWidth / 15,
                        child: Text(
                          "Cumulative to this semester",
                          style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                ],
              ),
              Row(
                children: [
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: (max_LanWidth / 2) / 3.5,
                        color: Colors.black38,
                        height: max_PorWidth / 15,
                        child: Text(
                          "C.Register\n21",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: (max_LanWidth / 2) / 4.7,
                        color: Colors.black26,
                        height: max_PorWidth / 15,
                        child: Text(
                          "C.Earn\n21",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: (max_LanWidth / 2) / 6,
                        color: Colors.black38,
                        height: max_PorWidth / 15,
                        child: Text(
                          "CA\n21",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: (max_LanWidth / 2) / 6,
                        color: Colors.black26,
                        height: max_PorWidth / 15,
                        child: Text(
                          "GP\n69",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: (max_LanWidth / 2) / 6,
                        color: Colors.black38,
                        height: max_PorWidth / 15,
                        child: Text(
                          "GPA\n3.29",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: (max_LanWidth / 2) / 3.5,
                        color: Colors.black26,
                        height: max_PorWidth / 15,
                        child: Text(
                          "C.Register\n93",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: (max_LanWidth / 2) / 4.7,
                        color: Colors.black12,
                        height: max_PorWidth / 15,
                        child: Text(
                          "C.Earn\n93",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: (max_LanWidth / 2) / 6,
                        color: Colors.black26,
                        height: max_PorWidth / 15,
                        child: Text(
                          "CA\n93",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: (max_LanWidth / 2) / 6,
                        color: Colors.black12,
                        height: max_PorWidth / 15,
                        child: Text(
                          "GP\n319",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: (max_LanWidth / 2) / 6,
                        color: Colors.black26,
                        height: max_PorWidth / 15,
                        child: Text(
                          "GPA\n3.43",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ])));
  }
}
